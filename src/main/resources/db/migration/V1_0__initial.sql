CREATE EXTENSION IF NOT EXISTS "uuid-ossp" SCHEMA "public";

create table if not exists car_statuses(
    id bigserial primary key,
    "name" varchar not null,
    description varchar
);

create table if not exists cars(
    id bigserial primary key,
    "name" varchar not null,
    "number" varchar not null,
    external_id uuid unique not null ,
    "date" timestamp not null,
    problem_description varchar not null,
    owner_name varchar not null,
    owner_phone varchar not null,
    status bigint references car_statuses(id) on delete restrict on update cascade
);

create table if not exists mechanics(
    id bigserial primary key,
    "name" varchar not null,
    phone_number varchar unique not null,
    "position" varchar not null,
    car bigint references cars(id) on delete restrict on update cascade
);

insert into car_statuses(id, "name", description) values
    (1, 'BROKEN', 'Машина поступила в автосерсив, ожидание свободного механика'),
    (2, 'REPAIRING', 'Автомобиль ремонтируется'),
    (3, 'DONE', 'Автомобиль отремонтирован');

alter sequence car_statuses_id_seq restart with 4;