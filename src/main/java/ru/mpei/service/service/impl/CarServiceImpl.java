package ru.mpei.service.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mpei.service.mapper.CarMapper;
import ru.mpei.service.model.dto.CarDto;
import ru.mpei.service.model.dto.StatusDto;
import ru.mpei.service.model.entity.Car;
import ru.mpei.service.repository.CarRepository;
import ru.mpei.service.repository.CarStatusRepository;
import ru.mpei.service.repository.MechanicRepository;
import ru.mpei.service.service.CarService;

import java.util.UUID;

@Service
@RequiredArgsConstructor()
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final CarStatusRepository carStatusRepository;
    private final CarMapper mapper;
    private final MechanicRepository mechanicRepository;

    @Override
    public void saveCarInfo(CarDto dto) {
        Car car = mapper.toEntity(dto);
        car.setStatus(carStatusRepository.findById(1L).get());
        carRepository.save(car);
    }

    @Override
    public void deleteCarInfo(UUID externalId) {
        var car = carRepository.getCarByExternalId(externalId);
        var mechanic =mechanicRepository.getMechanicByCar(car);
        if(mechanic != null){
            mechanic.setCar(null);
            mechanicRepository.save(mechanic);
        }
        carRepository.delete(car);
    }

    @Override
    public StatusDto getCarStatus(UUID externalId) {
        var car = carRepository.getCarByExternalId(externalId);
        var status = new StatusDto(car.getStatus().getName());
        return status;
    }
}
