package ru.mpei.service.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mpei.service.mapper.MechanicMapper;
import ru.mpei.service.model.dto.MechanicDto;
import ru.mpei.service.model.dto.WorkDto;
import ru.mpei.service.model.entity.Car;
import ru.mpei.service.repository.CarRepository;
import ru.mpei.service.repository.CarStatusRepository;
import ru.mpei.service.repository.MechanicRepository;
import ru.mpei.service.service.MechanicService;

@Service
@RequiredArgsConstructor()
public class MechanicServiceImpl implements MechanicService {

    private final MechanicMapper mapper;
    private final CarRepository carRepository;
    private final CarStatusRepository statusRepository;
    private final MechanicRepository mechanicRepository;

    @Override
    public void saveMechanicInfo(MechanicDto dto) {
        var mechanic = mapper.toEntity(dto);
        mechanicRepository.save(mechanic);
    }

    @Override
    public void repairCar(WorkDto workDto) {
        var car = carRepository.getCarByExternalId(workDto.getExternalId());
        car.setStatus(statusRepository.findById(2L).get());
        carRepository.save(car);
        var mechanic = mechanicRepository.getMechanicByPhoneNumber(workDto.getMechanicPhoneNumber());
        mechanic.setCar(car);
        mechanicRepository.save(mechanic);
    }

    @Override
    public void repairedCar(WorkDto workDto) {
        var car = carRepository.getCarByExternalId(workDto.getExternalId());
        car.setStatus(statusRepository.findById(3L).get());
        carRepository.save(car);
        var mechanic = mechanicRepository.getMechanicByPhoneNumber(workDto.getMechanicPhoneNumber());
        mechanic.setCar(null);
        mechanicRepository.save(mechanic);
    }

    @Override
    public void deleteMechanic(String phoneNumber) {
        Car car = null;
        var mechanic = mechanicRepository.getMechanicByPhoneNumber(phoneNumber);
        if(mechanic.getCar() != null){
            car = carRepository.findById(mechanic.getCar().getId()).get();
        }
        if(car != null){
            car.setStatus(statusRepository.findById(1L).get());
            carRepository.save(car);
        }
        mechanicRepository.delete(mechanic);
    }
}
