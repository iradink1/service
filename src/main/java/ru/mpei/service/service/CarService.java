package ru.mpei.service.service;

import ru.mpei.service.model.dto.CarDto;
import ru.mpei.service.model.dto.StatusDto;

import java.util.UUID;

public interface CarService {

    void saveCarInfo(CarDto dto);

    void deleteCarInfo(UUID externalId);

    StatusDto getCarStatus(UUID externalId);
}
