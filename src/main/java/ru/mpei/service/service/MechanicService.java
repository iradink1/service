package ru.mpei.service.service;

import ru.mpei.service.model.dto.MechanicDto;
import ru.mpei.service.model.dto.WorkDto;

public interface MechanicService {

    void saveMechanicInfo(MechanicDto dto);

    void repairCar(WorkDto workDto);

    void repairedCar(WorkDto workDto);

    void deleteMechanic(String phoneNumber);
}
