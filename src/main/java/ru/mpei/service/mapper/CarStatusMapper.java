package ru.mpei.service.mapper;

import org.mapstruct.Mapper;
import ru.mpei.service.model.dto.CarStatusDto;
import ru.mpei.service.model.entity.CarStatus;

@Mapper(componentModel = "spring")
public interface CarStatusMapper {

    CarStatusDto toDto(CarStatus entity);

    CarStatus toEntity(CarStatusDto dto);
}
