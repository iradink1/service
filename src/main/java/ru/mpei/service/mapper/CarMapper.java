package ru.mpei.service.mapper;

import org.mapstruct.Mapper;
import ru.mpei.service.model.dto.CarDto;
import ru.mpei.service.model.entity.Car;

@Mapper(componentModel = "spring", uses = CarStatusMapper.class)
public interface CarMapper {

    CarDto toDto(Car entity);

    Car toEntity(CarDto dto);
}
