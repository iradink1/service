package ru.mpei.service.mapper;

import org.mapstruct.Mapper;
import ru.mpei.service.model.dto.MechanicDto;
import ru.mpei.service.model.entity.Mechanic;

@Mapper(componentModel = "spring", uses = CarMapper.class)
public interface MechanicMapper {

    MechanicDto toDto(Mechanic entity);

    Mechanic toEntity(MechanicDto dto);
}
