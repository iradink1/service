package ru.mpei.service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.mpei.service.model.dto.MechanicDto;
import ru.mpei.service.model.dto.WorkDto;
import ru.mpei.service.service.MechanicService;

import java.util.UUID;

@RestController
@RequestMapping("/mechanic")
@RequiredArgsConstructor()
public class MechanicController {

    private final MechanicService service;

    @PostMapping(value = "/add")
    public void addMechanic(@RequestBody MechanicDto dto) {
        service.saveMechanicInfo(dto);
    }

    @PutMapping(value = "/{externalId}/repair")
    public void repairCar(@RequestBody WorkDto workDto) {
        service.repairCar(workDto);
    }

    @PutMapping(value = "/{externalId}/repair/done")
    public void repairedCar(@RequestBody WorkDto workDto) {
        service.repairedCar(workDto);
    }

    @DeleteMapping(value = "/{phoneNumber}/detele")
    public void deleteMechanic(@PathVariable String phoneNumber) {
        service.deleteMechanic(phoneNumber);
    }
}
