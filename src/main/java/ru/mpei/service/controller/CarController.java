package ru.mpei.service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mpei.service.model.dto.CarDto;
import ru.mpei.service.model.dto.StatusDto;
import ru.mpei.service.service.CarService;

import java.util.UUID;

@RestController
@RequestMapping("/car")
@RequiredArgsConstructor()
public class CarController {

    private final CarService service;

    @PostMapping(value = "/add")
    public void addCar(@RequestBody CarDto dto) {
        service.saveCarInfo(dto);
    }

    @DeleteMapping(value = "/{externalId}/detele")
    public void deleteCar(@PathVariable UUID externalId) {
        service.deleteCarInfo(externalId);
    }

    @GetMapping(value = "/{externalId}/status")
    public StatusDto getStatus(@PathVariable UUID externalId) {
        return service.getCarStatus(externalId);
    }
}
