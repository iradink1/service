package ru.mpei.service.model.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CarDto {

    private String name;
    private String number;
    private String problemDescription;
    private UUID externalId;
    private String ownerName;
    private String ownerPhone;
}
