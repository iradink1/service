package ru.mpei.service.model.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class WorkDto {
    UUID externalId;
    String mechanicPhoneNumber;
}
