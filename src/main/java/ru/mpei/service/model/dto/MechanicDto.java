package ru.mpei.service.model.dto;

import lombok.Data;

@Data
public class MechanicDto {

    private String name;
    private String phoneNumber;
    private String position;
}
