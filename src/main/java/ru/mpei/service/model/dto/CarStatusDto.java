package ru.mpei.service.model.dto;

import lombok.Data;

@Data
public class CarStatusDto {

    private Long id;
    private String name;
    private String description;
}
