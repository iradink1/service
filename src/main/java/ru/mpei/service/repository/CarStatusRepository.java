package ru.mpei.service.repository;

import org.springframework.data.repository.CrudRepository;
import ru.mpei.service.model.entity.CarStatus;

public interface CarStatusRepository extends CrudRepository<CarStatus, Long> {
}
