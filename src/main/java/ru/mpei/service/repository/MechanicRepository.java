package ru.mpei.service.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.mpei.service.model.entity.Car;
import ru.mpei.service.model.entity.Mechanic;

import java.util.Optional;

public interface MechanicRepository extends CrudRepository<Mechanic, Long> {

    @Query(value = "" +
            "SELECT *" +
            "     FROM mechanics" +
            "     WHERE phone_number = :phoneNumber" +
            "     FOR UPDATE skip locked;",
            nativeQuery = true)
    Mechanic getMechanicByPhoneNumber(@Param("phoneNumber") String phoneNumber);

    @Query(value = "" +
            "SELECT *" +
            "     FROM mechanics" +
            "     WHERE car = :car" +
            "     FOR UPDATE skip locked;",
            nativeQuery = true)
    Mechanic getMechanicByCar(@Param("car") Car car);
}
