package ru.mpei.service.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.mpei.service.model.entity.Car;

import java.util.UUID;

public interface CarRepository extends CrudRepository<Car, Long> {

    @Query(value = "" +
            "SELECT *" +
            "     FROM cars" +
            "     WHERE external_id = :externalId" +
            "     ORDER BY date" +
            "     FOR UPDATE skip locked;",
            nativeQuery = true)
    Car getCarByExternalId(@Param("externalId") UUID externalId);
}
